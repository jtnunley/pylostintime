# This file is part of Lost in Time.
# 
# Lost in Time is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# Lost in Time is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with Lost in Time.  If not, see <http://www.gnu.org/licenses/>.

# do bitwise operations on numbers

# split byte into upper and lower halves
def splitByte(b):
    lowerHalf = b & 15
    upperHalf = (b >> 4) & 15 
    return [upperHalf,lowerHalf]

# combine two byte halves into one
def combineByte(lower,upper):
    b = 0;
    lower = lower & 15
    upper = (upper >> 4) & 15
    b = b | lower
    b = b | upper
    return b

# read binary into string
def readBinaryString(stream):
    s = bytearray()
    info = stream.read(1)
    while info != b'\n':
        s.append(info[0])
        info = stream.read(1)
    return s.decode("utf-8")
