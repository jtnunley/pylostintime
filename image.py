# This file is part of Lost in Time.
# 
# Lost in Time is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# Lost in Time is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with Lost in Time.  If not, see <http://www.gnu.org/licenses/>.

# loading images

# python libraries
import math

# our libraries
from byteops import *
from color import Color
from error import error
from litutil import doNothing

class Image:
    def __init__(self,width=0,height=0):
        self.width = width
        self.height = height
    def loadf(self,stream,dependencies=[]):
        # read magic bytes
        info = stream.read(6)
        if not info == b'LIT50\n': error("Image is not a valid LCIF image")

        # read line, convert to string, that's the id
        self.id = readBinaryString(stream) 

        # next three bytes will be width, height, and size of color pallete
        info = stream.read(3)
        self.width = info[0]
        self.height = info[1]
        self.palletteLength = info[2]

        # idiot proofing
        if self.width <= 0 or self.height <= 0:
            error("Image cannot have width or height of zero",False)
        if self.palletteLength <= 0 or self.palletteLength >= 16:
            error("Pallette length out of range",False)

        # next [palletteLength*3] bytes will be colors, in RGB order
        self.pallette = []
        for i in range(0,self.palletteLength):
            info = stream.read(3)
            self.pallette.append(Color(info[0],info[1],info[2]))

        # skip to next newline
        while stream.read(1) != b'\n':
            doNothing()

        # read in rows of bytes
        rowLen = int(math.ceil(self.width / 2))
        self.imageData = list()
        for i in range(0,self.height):
            # read in one row of bytes    
            info = stream.read(rowLen)  
            self.imageData.append(list())
            for b in info:  # per byte ops
                if b != b'':
                    byteHalves = splitByte(b)
                    self.imageData[i].append(byteHalves[0])
                    self.imageData[i].append(byteHalves[1])
            # read to newline
            while stream.read(1) != b'\n':
                doNothing()

    def load(self,filename):
        with open(filename,'rb') as f:
            self.loadf(f)
    def savef(self,stream):
        # save magic bytes
        stream.write(b'LIT50\n')

        # write image id
        stream.write(self.id.encode(encoding="utf=8"))
        stream.write(b'\n')
        
        # next three bytes are width, height, and pallete length
        info = bytearray([self.width,self.height,self.palletteLength])
        stream.write(info)

        # write pallette data
        for i in range(0,self.palletteLength):
            color = self.pallette[i]
            info = bytearray([color.red,color.green,color.blue])
            stream.write(info)

        # write newline
        stream.write(b'\n')
        
        # write out byte rows
        rowLen = int(math.ceil(self.width / 2))
        for row in self.imageData:
            infoArr = []
            for i in range(0,rowLen,2):
                infoArr.append(combineByte(row[i],row[i + 1]))
            infoArr.append(ord('\n'))
            stream.write(bytearray(infoArr))

    def save(self,filename):
        with open(filename,'wb') as f:
            self.savef(f)            

    def getpixel(self,x,y):
        index = self.imageData[y][x]
        return self.pallette[index - 1]
