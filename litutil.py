# This file is part of Lost in Time.
# 
# Lost in Time is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# Lost in Time is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with Lost in Time.  If not, see <http://www.gnu.org/licenses/>.

# hold various useful utility functions

from error import error

# dummy function to do nothing, good for loops w/ null statment
def doNothing():
    if False:
        error("False is now True",False)
