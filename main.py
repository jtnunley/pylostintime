#!/usr/bin/python

# This file is part of Lost in Time.
# 
# Lost in Time is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# Lost in Time is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with Lost in Time.  If not, see <http://www.gnu.org/licenses/>.

from datafile import Datafile
from graphics import *
import image
import color
import renderer

def main():
    print("Lost in Time vALPHA.0.2\n") 
    Datafile.current_datafile = Datafile()
    Datafile.current_datafile.load("datafile.litg")
    df = Datafile.current_datafile

    # load map and print
    for ds in df.datachunks:
        print(ds.pos)
        print(ds.name)
        print(ds.type)
    
    ds = df.loaddata("map_first")
    mp = ds.value

    for dushh in mp.palletedata:
        print("[%i,%i,%i]" % (dushh.red,dushh.green,dushh.blue))

    print()
    txt = ""
    for row in mp.mapdata:
        for pixel in row:
            if pixel == 2:
                txt += "+"
            else:
                txt += " "
        print(txt)
        txt = ""

    # colorBuffer.append(color.ColorRect(color.Color(0.0,255.0,0.0),10,10,100,100))
    # colorBuffer.append(color.ColorRect(color.Color(255.0,0.0,0.0),10,300,200,100))
    # colorBuffer.append(color.ColorRect(color.Color(85.0,26.0,139.0),310,10,200,100))
    # colorBuffer.append(color.ColorRect(color.Color(255.0,105.0,180.0),310,300,200,100))

    renderFuncs.append(lambda: renderer.renderMap(mp))
    
    initializeOpenGL()
    print("Test")
    
if __name__ == "__main__":
    main()
