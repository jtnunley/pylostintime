# This file is part of Lost in Time.
# 
# Lost in Time is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# Lost in Time is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with Lost in Time.  If not, see <http://www.gnu.org/licenses/>.

# datafiles, et all

# stdlib python libs
from enum import Enum

# our files
from byteops import readBinaryString
from image import Image
from map import Map

class Datachunk:
    def __init__(self,pos,name,type):
        self.pos = pos
        self.name = name
        self.type = type
        self.dependencies = list()
        self.value = None
    
class Datafile:
    def __init__(self):
        self.datachunks = list()
    def loadf(self,stream):
        # veryify magic bytes
        info = stream.read(6)
        if info != b'LIT01\n':
            error("File is not a valid datafile",False)

        # read in motd
        self.motd = readBinaryString(stream)

        current = None
        while True:
            info = stream.read(1)
            if info:
                if info == b'L':
                    info = stream.read(5)
                    if info == b'IT50\n':
#                        print("Found Image")
                        current = Datachunk(stream.tell() - 6,readBinaryString(stream),'Image') 
                        self.datachunks.append(current)
                    elif info == b'IT49\n':
#                        print("Found Map")
                        current = Datachunk(stream.tell() - 6,readBinaryString(stream),'Map')
                    elif info == b'IT03\n':
#                        print("Found Dependencies")
                        dependencyLen = (stream.read(1))[0]
                        for i in range(0,dependencyLen):
                            current.dependencies.append(readBinaryString(stream))
                        self.datachunks.append(current)
            else:
                break
    def load(self,filename):
        with open(filename,'rb') as f:
            self.loadf(f)
        self.filename = filename
    def loaddata(self,id):
        with open(self.filename,'rb') as f:
            for chunk in self.datachunks:
                if chunk.name == id:
                    f.seek(chunk.pos)
                    value = None

                    # load dependencies
                    deps = []
                    for dependency in chunk.dependencies:
                        deps.append(self.loaddata(dependency).value)

                    if chunk.type == 'Image':
                        value = Image()
                    elif chunk.type == 'Map':
                        value = Map()
                    value.loadf(f,deps)

                    chunk.value = value
                    return chunk
        return None

    current_datafile = None

