# This file is part of Lost in Time.
# 
# Lost in Time is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# Lost in Time is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with Lost in Time.  If not, see <http://www.gnu.org/licenses/>.

# running graphics library for OpenGL

# opengl libs
from OpenGL.GLUT import *
from OpenGL.GLU import *
from OpenGL.GL import *

# for argv
import sys
import color

# width, height, and window pointer
windowWidth = 600
windowHeight = 480
window = 0

# color array
colorBuffer = list()

# tell opengl to draw in 2d
def refresh2d(width, height):
    glViewport(0, 0, width, height)
    glMatrixMode(GL_PROJECTION)
    glLoadIdentity()
    glOrtho(0.0, width, 0.0, height, 0.0, 1.0)
    glMatrixMode (GL_MODELVIEW)
    glLoadIdentity()

def drawRectTrue(x, y, width, height):
    glBegin(GL_QUADS)                                  # start drawing a rectangle
    glVertex2f(x, y)                                   # bottom left point
    glVertex2f(x + width, y)                           # bottom right point
    glVertex2f(x + width, y - height)                  # top right point
    glVertex2f(x, y - height)                          # top left point
    glEnd() 

# function to render a color point
def drawRect(colorrect):    
    global windowHeight
    c = colorrect.color

    # convert color to opengl values
    red = (c.red)/255
    green = (c.green)/255
    blue = (c.blue)/255

    #print(red)
    #print(green)
    #print(blue)
    
    # for opengl, y starts at the bottom
    # for our system, y starts at the top
    # use math to translate
    trueY = windowHeight - colorrect.y
    
    # set color
    # glColor(red,green,blue,1.0)
    
    # set point size
    # glPointSize(5.0)
    
    # draw point
    glColor3f(red,green,blue)
    drawRectTrue(colorrect.x, trueY, colorrect.width, colorrect.height)

# function to draw on window
def display():
    global windowWidth
    global windowHeight
    glClearColor(0.0,0.0,0.0,1.0)
    glClear(GL_COLOR_BUFFER_BIT)
    glLoadIdentity()
    refresh2d(windowWidth,windowHeight)

    for func in renderFuncs:
        func()
    for rect in colorBuffer:
        drawRect(rect) 
    colorBuffer.clear()

    glutSwapBuffers()

# list of functions to call when rendering
renderFuncs = list()

# function called when resizing window, update windowWidth and windowHeight
def reshape(newW, newH):
    global windowWidth
    global windowHeight
    windowWidth = newW
    windowHeight = newH

# function to initialize the window
def initializeOpenGL():
    glutInit(sys.argv)
    # glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH)
    glutInitWindowSize(windowWidth,windowHeight)
    glutInitWindowPosition(50,50)
    glutCreateWindow("Lost in Time")

    glutDisplayFunc(display)
    glutReshapeFunc(reshape)
    glutMainLoop()


