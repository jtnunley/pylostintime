# This file is part of Lost in Time.
# 
# Lost in Time is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# Lost in Time is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with Lost in Time.  If not, see <http://www.gnu.org/licenses/>.

# renders things into the opengl window

import color
import graphics
from map import Map

pixelsPerGlob = 4
globsPerTile = 8
tilesHorizontal = 20
tilesVertical = 16
tileWidth = 30
tileHeight = 30

def calculateTileWH():
    global tileWidth
    global tileHeight
    tileWidth = graphics.windowWidth / tilesHorizontal
    tileHeight = graphics.windowHeight / tilesVertical

# render a map
def renderMap(map):
    calculateTileWH()
    i = 0
    j = 0
    for row in map.mapdata:
        for pixel in row:
            if pixel == 0:
                break
            c = map.palletedata[pixel - 1]
            clrr = color.ColorRect(c,j * tileWidth,i * tileHeight,tileWidth,tileHeight)
            graphics.colorBuffer.append(clrr)
            j += 1
        j = 0
        i += 1
            
