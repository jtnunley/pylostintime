# This file is part of Lost in Time.
# 
# Lost in Time is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# Lost in Time is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with Lost in Time.  If not, see <http://www.gnu.org/licenses/>.

# loading and saving maps

from byteops import readBinaryString
from error import error
from tile import Tile

class Map:
    def __init__(self,width=0,height=0):
        self.width = width
        self.height = height
    def loadf(self,stream,dependencies=[]):
        # check magic bytes
        info = stream.read(6)
        if info != b'LIT49\n':
            error("File is not a valid map file")
        
        # read map id
        self.id = readBinaryString(stream)

        # read dependencies
        # note: first dependency is the map's tile data (in the form of an image). all further dependencies will be entities
       
        # read dependency signifier
        info = stream.read(6)
        if info != b'LIT03\n':
            error("Map object must have at least one dependency",False)
 
        # read dependency count
        self.dependencyCount = stream.read(1)[0]

        # read dependency names
        self.dependencies = []
        for i in range(0,self.dependencyCount):
            self.dependencies.append(readBinaryString(stream))

        # TODO: get datachunk systems up, read data from dataimage
        imgchunk = dependencies[0]
        self.mapdata = imgchunk.imageData
        self.palletedata = imgchunk.pallette
    def load(self,filename):
        with open(filename,'rb') as stream:
            self.loadf(stream)
